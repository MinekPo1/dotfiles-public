"## General ##

" filetype
filetype plugin on
filetype indent on
" autoreload on ext write
set autoread
au FocusGained,BufEnter * checktime

set wildmenu


"## Feel ##

set foldmethod=indent

" add leader
let mapleader = ","

" :W -> sudo save
command! W execute 'w !sudo tee % > /dev/null' <bar> edit!

" ,w fast save
nmap <leader>w :w!<cr>

" :Q -> :q!

command! Q execute "q!"

" C-jkhl move tabs
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Alt-up/down move line
" Yanked from https://vim.fandom.com/wiki/Moving_lines_up_or_down
nnoremap <A-down> :m .+1<CR>==
nnoremap <A-up> :m .-2<CR>==
inoremap <A-down> <Esc>:m .+1<CR>==gi
inoremap <A-up> <Esc>:m .-2<CR>==gi
vnoremap <A-down> :m '>+1<CR>gv=gv
vnoremap <A-up> :m '<-2<CR>gv=gv

" move screen
map <S-down> <C-e>
map <S-up> <C-y>

"## chezmoi ##

autocmd BufWritePost ~/.local/share/chezmoi/* ! chezmoi apply --source-path "%"

"## Python settings ##
" set indent type to tabs

au FileType python set noet sw=8 tabstop=8 softtabstop=8

"## Markdown settings ##

" set indent to two spaces
au FileType markdown set et sw=2 tabstop=2 softtabstop=2

"## Coc ##
" C-Space -> coc completion
inoremap <silent><expr> <c-space> coc#pum#visible() ? coc#pum#confirm()

nnoremap <silent> <C-a> :call CocActionAsync('doHover')<CR>

"## Look ##
set number
set relativenumber
set so=5

set list
set listchars=space:.,nbsp:\,,tab:\ \ \|,trail:-,leadmultispace:\ \ .\| 

set title
set titlestring=nvim\ %f

set linebreak
set showbreak=└>\ 
set breakindent
set breakindentopt=min:20,list:-1

set formatlistpat=^\\s*\\(\\d\\+\\.\\\|[-+*]\\s\\)\\s*

set statusline=%-32(%y\ %t%(\ %M%)%)%=%{strftime('%y-%m-%d\ %H:%M')}%=%-28(@%l:%c%V%)\ %P

"## Plugins ##

call plug#begin()

Plug 'editorconfig/editorconfig-vim'

" node host
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Python
" Plug 'davidhalter/jedi-vim'
" Plug 'hdima/python-syntax'

" Plug 'integralist/vim-mypy'

Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'

Plug 'mattn/emmet-vim'

Plug 'alaviss/nim.nvim'

Plug 'udalov/kotlin-vim'

Plug 'Yggdroot/indentLine'

Plug 'edluffy/hologram.nvim'

Plug 'khaveesh/vim-fish-syntax'

call plug#end()




























